#include <cstdlib>
#include <mpi.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

using namespace std;

int main(int argc, char** argv) {

    int i, rank, size, namelen;
    char name[MPI_MAX_PROCESSOR_NAME];
    MPI_Status stat;

    MPI_Init (&argc, &argv);

    MPI_Comm_size (MPI_COMM_WORLD, &size);
    MPI_Comm_rank (MPI_COMM_WORLD, &rank);
    MPI_Get_processor_name (name, &namelen);

    printf("Process: %d from %d on %s\n", rank, size, name);
    for(i=0; i<argc; i++) printf("argc=%d argv=%s\n", argc, argv[i]);
    
/*
    if (rank == 0) {

	printf ("Start waiting: rank %d of %d running on %s\n", rank, size, name);      

	for (i = 1; i < size; i++) {

	    MPI_Recv (&rank, 1, MPI_INT, i, 1, MPI_COMM_WORLD, &stat);
	    MPI_Recv (&size, 1, MPI_INT, i, 1, MPI_COMM_WORLD, &stat);
	    MPI_Recv (&namelen, 1, MPI_INT, i, 1, MPI_COMM_WORLD, &stat);
	    MPI_Recv (name, namelen + 1, MPI_CHAR, i, 1, MPI_COMM_WORLD, &stat);
	    printf ("Returned data from: rank %d of %d running on %s\n", rank, size, name);
	}
    } else {

sleep(5);
	MPI_Send (&rank, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
	MPI_Send (&size, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
	MPI_Send (&namelen, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
	MPI_Send (name, namelen + 1, MPI_CHAR, 0, 1, MPI_COMM_WORLD);
    }
*/

    if(rank == 0) {
        for (int i = 0; i < 100; i++) {
            printf("%d %d\n", rank, i);
            sleep(2);
        }
    }
    else {
        for (int i = 0; i < 100; i++) {
            printf("%d %d\n", rank, i);
            sleep(2);
        }
    }
    
/*
    for (int i = 0; i < 10; i++) {
        printf("%d %d\n", rank, i);
        sleep(1);
    }
*/
    MPI_Finalize ();
     
    return 0;
}
